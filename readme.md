[[_TOC_]]


Ce guide permet d'écrire et de modifier les extensions JupyterLab présentes dans le dossier parent ainsi que de les compiler pour JupyterLite.

## Création d'un environnement de travail

Qu'il s'agisse de créer une nouvelle extension ou bien d'en modifier une, une bonne pratique consiste à créer un environnement. Plusieurs méthodes sont possibles. Ici, on utilise [conda](https://conda.io/projects/conda/en/latest/index.html), un gestionnaire de paquets et d'environnements. La procédure d'installation de conda est [ici](https://conda.io/projects/conda/en/latest/user-guide/install/index.html).

Afin de créer l'environnement de travail local, on suit le [tutoriel de JupyterLab](https://jupyterlab.readthedocs.io/en/latest/extension/extension_tutorial.html#install-nodejs-jupyterlab-etc-in-a-conda-environment). Les commandes suivantes créent l'environnement et installent les outils nécessaires. 

Dans le cadre d'une modification des extensions extistantes, il est conseillé de créer deux environnements distincts :
- l'un pour le mode édition (RW)
- l'autre pour le mode lecture (RO)

En effet, les compilations pour JupyterLite ayant lieu dans des dossiers différents selon le mode d'édition, cette procédure permet de prévenir la compilation d'extensions erronées.

```bash 
conda create -n jl-lnb-rw-env --override-channels --strict-channel-priority -c conda-forge -c nodefaults jupyterlab=3.4 retrolab cookiecutter nodejs pkginfo
conda activate jl-lnb-rw-env
```

La commande suivante permet de s'assurer que l'installation s'est bien déroulée :
```bash
jupyter labextension list
```

## Modification d'une extension JupyterLab présente sur https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/jupyter/

On suppose que l'environnement de travail est créé et activé.

On se place dans un dossier local et on clone l'extension souhaitée (l'exemple qui suit clone l'extension lnbtheme):
```bash
mkdir jupyterlab-extensions
cd jupyterlab-extensions
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/jupyter/lnbtheme.git
```

On installe l'extension en mode développement puis on recompile :
```bash
cd lnbtheme
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build
```

JupyterLab peut être lancé via à la commande :
```bash
jupyter lab
```

Dans un navigateur, la page `http://localhost:8888/retro/tree` devrait afficher dans sa console que l'extension est activée.

Le code source de l'extension est présent dans le dossier `lnbtheme/src`. Il est possible de le modifier puis de re-compiler :
```bash
cd lnbtheme
jlpm run build
```

(d'éventuelles dépendances sont à ajouter auparavant via la commande `jlpm add @<dépendance>`)

## Création d'une extension JupyterLab

On suppose que l'environnement de travail est créé et activé.

On se place dans un dossier de travail puis on crée les fichiers de base de l'extension à l'aide de [cookiecutter](https://github.com/cookiecutter/cookiecutter).

```bash 
mkdir jupyterlab-extensions
cd jupyterlab-extensions
cookiecutter https://github.com/jupyterlab/extension-cookiecutter-ts
```

```bash 
Select kind:
1 - frontend
2 - server
3 - theme
Choose from 1, 2, 3 [1]: 1
author_name [My Name]:
author_email [me@test.com]: 
labextension_name [myextension]: lnbtheme
python_name [lnbtheme]: lnbtheme
project_short_description [A JupyterLab extension.]: A minimalist them for LabNbook
has_settings [n]: n
has_binder [n]: n
test [y]: n
repository [https://github.com/github_username/lnbtheme]:
```

On installe l'extension en mode développement puis on recompile :
```bash
cd lnbtheme
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build
```

JupyterLab peut être lancé via à la commande :
```bash
jupyter lab
```

Dans un navigateur, la page `http://localhost:8888/retro/tree` devrait afficher dans sa console que l'extension est activée.

Le code source de l'extension est présent dans le dossier `lnbtheme/src`. Il est possible de le modifier puis de re-compiler :
```bash
cd lnbtheme
jlpm run build
```

(d'éventuelles dépendances sont à ajouter auparavant via la commande `jlpm add @<dépendance>`)

## Compilation pour JupyterLite

Il faut désormais créer l'extension JupyterLite. Pour cela, on installe jupyterlite dans l'environnement de travail :

```bash
pip install jupyterlite=0.1.0b18
```

On se place ensuite dans un dossier quelconque puis on lance la compilation de l'extension pour JupyterLite dans ce dossier.

Attention, 
- `lnb_ro_theme` et `lnb_ro_init` sont compilées dans le dossier `lite_ro`.
- `lnb_rw_theme`, `lnb_rw_init` et `lnb_rw_save` sont compilées dans le dossier `lite_rw`.

```bash
mkdir examples
cd examples
jupyter lite build --output-dir lite_rw --pyodide https://github.com/pyodide/pyodide/releases/download/0.22.0/pyodide-0.22.0.tar.bz2
```

Les lignes suivantes témoignent de la bonne prise en compte de l'extension dans la compilation :

```
...
federated_extensions:copy:ext:jupyterlab-iframe-bridge-example
.  pre_build:federated_extensions:copy:ext:jupyterlab-iframe-bridge-example
...
```

Un dossier `examples/lite/` contenant tout le nécessaire au fonctionnement de JupyterLite est créé (on notera la présence de notre extension dans le sous-répertoire `extensions`).

:warning: En attendant de touver une solution plus automatisée, il faut retirer une extension en ajoutant les lignes suivantes dans le fichier de configuration `lite_ro/jupyter-lite.json` ou bien `lite_rw/jupyter-lite.json` :

```
...
{
    "jupyter-config-data": {
        ...
        "disabledExtensions": [
            "@jupyterlab/cell-toolbar-extension:plugin",
            "@jupyterlite/server-extension:service-worker"
        ],
        ...
...
```

## Test de l'extension

Afin de tester le bon fonctionnement de l'extension dans JupyterLite, on lance un serveur minimaliste depuis le dossier `examples` :

```bash
cd examples
python -m http.server -b 127.0.0.1
```

La vérification s'opère dans un navigateur, à l'adresse `http://127.0.0.1:8000`.

## Quelques raccourcis pour gagner du temps

Les commandes `./lnb update_jupyterlite` et `./lnb build_jupyterlite` permettent respectivement de mettre à jour les sources depuis les dépôts correspondants et de réaliser le build automatiquement (quelques minutes d'exécution).

En pratique, pour modifier une extension, on peut exécuter la première commande, effectuer les modifications souhaitées puis exécuter la seconde commande.

## Difficultés connues

La commande `pip install -ve .` peut parfois échouer en raison de conflits liés à npm. Pour éviter cela, on a placé `skipLibCheck": true` dans le fichier `tsconfig.json` ([référence](https://discourse.jupyter.org/t/struggling-with-extensions-and-dependencies-versions/19550)).
